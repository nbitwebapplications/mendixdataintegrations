﻿using JobClients;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MendixDataIntegration
{
    class Program
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        static void Main(string[] args)
        {
            var txtFolder = ConfigurationManager.AppSettings["txt_file_location"];
            var mendixUrl = ConfigurationManager.AppSettings["mendix_csv_endpoint"];
            var mendixUser = ConfigurationManager.AppSettings["ws_user"];
            var mendixPassword = ConfigurationManager.AppSettings["ws_password"];
            MendixCsvClient mendixClient = new MendixCsvClient(mendixUrl, mendixUser, mendixPassword);

            Dictionary<string, KeyEntityMapping> fnameToEntity = new Dictionary<string, KeyEntityMapping> {
                {"aldon_aurora_detail", new KeyEntityMapping("TempAuroraDetailObject",  "ObjectName")},
                {"aldon_aurora_header", new KeyEntityMapping("TempAldonTask","TaskKey") },
                {"aldon_diver_header", new KeyEntityMapping("TempAldonDiverTask","ObjectKey") },
                {"aldon_diver_detail", new KeyEntityMapping("TempDiverDetailObject","PartKey") },
                {"aldon_wm_header", new KeyEntityMapping("TempWMHeader","TASK_KEY") },
                {"aldon_wm_detail", new KeyEntityMapping("TempWMDetail","OBJECT_NAME") },
            };

            Task.Run(async () =>
            {
                log.Info("Starting job...");
                var files = Directory.GetFiles(txtFolder, "*.txt");
                foreach (var file in files)
                {
                    List<string> keys = new List<string>();
                    var fname = Path.GetFileNameWithoutExtension(file).ToLower();
                    if (!fnameToEntity.ContainsKey(fname))
                        continue;

                    log.Debug($"Converting {Path.GetFileName(file)} to .csv");

                    var ke = fnameToEntity[fname];
                    var csv = HelperFns.MakeCsvFromTxt(file, ke.Key, keys);
                    log.Debug($"Includes keys: {String.Join(",", keys)}");
                    var mres = await mendixClient.SendCsvFile(csv, ke.Entity);
                    if (mres.IsOkay)
                    {
                        log.Debug($"{mres.LinesProcessed} lines processed");
                        File.Delete(file);
                    }
                    else
                    {
                        log.Error($"Error sending csv to {mendixUrl}");
                        break;
                    }
                }
                log.Info("Job completed.");
            }).GetAwaiter().GetResult();

        }
    }

    public class KeyEntityMapping
    {
        public KeyEntityMapping(string entity, string key)
        {
            _entity = entity;
            _key = key;
        }
        private string _entity;
        private string _key;
        public string Key
        {
            get
            {
                return _key;
            }
        }
        public string Entity
        {
            get
            {
                return _entity;
            }
        }
    }
}
