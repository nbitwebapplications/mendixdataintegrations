﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JobClients
{
    public class MendixCsvServicesResponse
    {
        private JObject baseObj;
        public MendixCsvServicesResponse(string jobjstr)
        {
            baseObj = JObject.Parse(jobjstr);
        }
        public string Status
        {
            get
            {
                return baseObj["status"].ToString();
            }
        }
        public bool IsOkay
        {
            get
            {
                return Status == "successfully created objects";
            }
        }
        public int LinesProcessed
        {
            get
            {
                return (int)baseObj["lines_processed"];
            }
        }
    }
}
