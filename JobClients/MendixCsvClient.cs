﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace JobClients
{
    public class MendixCsvClient
    {
        private HttpClient client;

        public MendixCsvClient(string baseUrl, string user, string password)
        {
            client = new HttpClient();
            client.BaseAddress = new Uri(baseUrl);

            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;
            
            var authenticationString = $"{user}:{password}";
            var base64EncodedAuthenticationString = Convert.ToBase64String(System.Text.ASCIIEncoding.ASCII.GetBytes(authenticationString));
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", base64EncodedAuthenticationString);
            client.DefaultRequestHeaders.Accept.Clear();
        }
        public async Task<MendixCsvServicesResponse> SendCsvFile(string csvPath, string entity)
        {

            StringContent content = new StringContent(File.ReadAllText(csvPath));
            content.Headers.ContentType = new MediaTypeHeaderValue("text/csv");

            HttpRequestMessage msg = new HttpRequestMessage(HttpMethod.Post, client.BaseAddress + entity);
            msg.Content = content;
            var res = await client.SendAsync(msg);
            var resc = await res.Content.ReadAsStringAsync();
            MendixCsvServicesResponse mres = new MendixCsvServicesResponse(resc);
            return mres;
        }
    }
}
