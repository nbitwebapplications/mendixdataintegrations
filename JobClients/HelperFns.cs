﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace JobClients
{
    public static class HelperFns
    {
        public static string MakeCsvFromTxt(string fname, string key, List<string> keys)
        {
            Regex rexBadChars = new Regex("[^\x20-\x7E]", RegexOptions.IgnoreCase);
            StreamReader sr = new StreamReader(fname);
            string fpath = Path.GetDirectoryName(fname);
            string fnameNoExt = Path.GetFileNameWithoutExtension(fname);
            string csvPath = Path.Combine(fpath, fnameNoExt + ".csv");
            StreamWriter sw = new StreamWriter(csvPath, false, Encoding.UTF8);
            var firstLine = sr.ReadLine();
            firstLine = firstLine.Replace("\t\t", "\t").Replace("\t", ",").Replace(" ", "").Replace(key, $"{key}*");
            var firstLineParts = firstLine.Split(new char[] { ',' }).ToList();
            var kpos = firstLineParts.IndexOf(key + "*");
            sw.WriteLine(firstLine);
            while (!sr.EndOfStream)
            {
                var line = sr.ReadLine();
                var lineParts = line.Split(new char[] { '\t' });
                keys.Add(lineParts[kpos].Trim());
                line = line.Replace(",", " ").Replace("\t", ",");
                line = rexBadChars.Replace(line, "_");
                sw.WriteLine(line);
                sw.Flush();
            }
            sw.Close();
            sr.Close();
            return csvPath;
        }
    }
}
